module.exports = function(grunt) {

  grunt.initConfig({
    subgrunt: 
    {
      purecss: 
      {
        'bower_components/purecss' : 'default'
      }
    },
    copy: 
    {
      main: 
      {
        files: [
        {expand: true, cwd: 'code/', src: ['js/**', 'css/**'], dest: 'build/'},
        {expand: true, cwd: 'code/html/', src: ['**'], dest: 'build/'}
        ]
      },
      libs: 
      {
        files: 
        [
          {expand: true, cwd: 'bower_components/purecss/build/',    src: ['*'],                 dest: 'build/libs/pure'},
          {expand: true, cwd: 'bower_components/font-awesome/',     src: ['css/*', 'fonts/*'],  dest: 'build/libs/font-awesome'},
          {expand: true, cwd: 'bower_components/ngDialog/',         src: ['css/*', 'js/*'],     dest: 'build/libs/ngDialog'},
          {expand: true, cwd: 'bower_components/angularjs/',        src: ['*'],                 dest: 'build/libs/angularjs'},
          {expand: true, cwd: 'bower_components/lodash/dist/',      src: ['*'],                 dest: 'build/libs/lodash'}
        ]
      }
    },
    clean:
    {
      options : 
      {
        force : true
      },
      main : ["build/*"]
    },
    watch: 
    {
      files : ['code/**', 'Gruntfile.js'],
      tasks : ['build-dev'],
      options : 
      {
        livereload : true
      }
    },
    connect:
    {
      devserv:
      {
        options: 
        {
          port: 1212,
          hostname: "localhost",
          base: "build",
          keepalive: true,
          livereload: true,
          debug: true,
          open: true
        }
      }
    },
    concurrent: 
    {
      dev: 
      {
        tasks: ['watch', 'connect'],
        options: 
        {
          logConcurrentOutput: true
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-subgrunt');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-concurrent');

  grunt.registerTask('build-dev', ['clean', 'copy:main', 'copy:libs']);
  grunt.registerTask('default', ['subgrunt', 'build-dev', 'concurrent:dev']);
};