if(typeof localStorage.version == "undefined")
{
	localStorage.version = JSON.stringify(new Date());
}
else
{
	var version = new Date(JSON.parse(localStorage.version));
	var current = new Date();
	if(current - version > 3600000)
	{
		localStorage.version = JSON.stringify(current);
		window.location.reload(true);
	}
}

var app = angular.module("main", ["taskModule", "trackerModule", "utilitiesModule", "statsModule", "ngDialog"]);

app.controller("mainController", 
	["$scope", "$interval", "$filter","taskService", "trackerService", "statsService", "ngDialog", function($scope, $interval, $filter, taskService, trackerService, statsService, ngDialog)
{
	$scope.current = taskService.get();
	$scope.trackers = trackerService.get();

	var save = function()
	{
		taskService.save($scope.current);
	}

	var archive = function(name, date)
	{
		$scope.current.name = name;
		$scope.current.date = date;
		taskService.archiveList($scope.current)
		.then(function(archived){		
			$scope.current = new taskService.taskList();
			save();
		});
	}

	$scope.assignTrackers = function()
	{
		$scope.choosing = true;
		$scope.assigningTrackers = true;
	}

	$scope.selectAll = function(mode){
		if(mode)
		{
			var visible = $filter('filter')($scope.current.tasks, $scope.filterTasks);
			visible.forEach(function(t){t.$selected = true;});
			return;
		}

		$scope.current.tasks.forEach(function(t){t.$selected = false;});
	}

	$scope.filterByTracker = function(tracker)
	{
		return function(task)
		{
			var value = false;
			
			task.trackers.forEach(function(t){ if(value){ return; } value = (t._id == tracker._id);});
			return value;
		}
	} 

	$scope.filterTasks = function(task)
	{
		if(typeof $scope.query == "undefined" || $scope.query == "")
		{
			return true;
		}

		if(task.name.toLowerCase().indexOf($scope.query.toLowerCase()) > -1)
		{
			return true;
		}

		task.$selected = false;
		return false;
	}

	$scope.buildStats = function()
	{
		var build = { stats: [], date: null};
		var stats = {};
		stats['global'] = new statsService.stats('Global', '#D4D4D4');
		$scope.current.trackers.forEach(function(tr){
			stats[tr._id] = new statsService.stats(tr.name, tr.color);
		});
		$scope.current.tasks.forEach(function(t){
			stats['global'].update(t);
			t.trackers.forEach(function(tr)
			{
				stats[tr._id].update(t); 
			})
		});
		$scope.current.trackers.forEach(function(tr){
			build.stats.push(stats[tr._id]);
		});
		build.stats = _.sortBy(build.stats, function(s){ return (s.correct.count - 0.25*s.incorrect.count)/s.all.count;})
		build.stats.push(stats['global']);
		build.date = new Date();
		$scope.current.statsBuild = build;
	}

	$scope.doneAssigningTrackers = function()
	{
		$scope.choosing = false;
		$scope.selectAllMode = false;
		$scope.assigningTrackers = false;
		$scope.current.tasks.forEach(function(t){ t.$selected = false; });
		$scope.current.trackers.forEach(function(t){ t.$selected = false; });
	}

	$scope.assignUnassignTracker = function(tracker, mode)
	{
		var selected = _.where($scope.current.tasks, "$selected");
		selected.forEach(function(t)
		{
			_.remove(t.trackers, function(tr){	return tr._id == tracker._id;	});
		})

		if(mode == 'assign')
		{
			selected.forEach(function(t){
				var trackers = t.trackers;
				trackers.push(tracker);
				trackers = _.sortBy(trackers, "_id");
				t.trackers = trackers;
			})
		}
	}

	$scope.addTask = function(tracker, running)
	{
		var task = new taskService.task("task #" + ($scope.current.tasks.length + 1));
		
		if(typeof tracker != "undefined")
		{
			task.trackers.push(tracker);
			task.name = task.name + " " + tracker.name;
		}
		if(running)
		{
			task.running = true;
		}

		$scope.current.tasks.splice(0, 0, task);
		save();				save();
	};

	$scope.removeTask = function(task)
	{
		_.remove($scope.current.tasks, function(t){ return t._id == task._id; });
		save();
	};

	$scope.addTracker = function(name)
	{
		if(typeof name == "undefined" || name.trim() == "")
		{
			$scope.addTrackerError = "Must define a name for the tracker";
			return;
		}
		name = name.trim();

		if(_.some($scope.current.trackers, function(t){ return t.name.toLowerCase() == name.toLowerCase(); }))
		{
			$scope.addTrackerError = "There is a tracker with that name";			
			return;
		}

		$scope.current.trackers.splice(0, 0, new trackerService.tracker(name, u.colors.get($scope.current.trackers.length)));
		$scope.addTrackerError = "";
		save();
	}

	$scope.openArchiveDialog = function()
	{
		ngDialog.open({
			template: 	'templates/archive-dialog.html',
			className: 	'ngdialog-theme-default ngdialog-theme-custom',
			controller: ["$scope", function(s){
							s.archive = function()
							{
								if(typeof s.date == "undefined")
								{
									s.note = "You must define a date.";
									return;
								}

								archive(s.name, s.date);
								
								s.closeThisDialog();
							}
							window.myscope = s;
						}],
			scope:   	$scope,
			cache: 		false
		});
	}

	$scope.$on("changes-made", function()
	{
		save();
	})
	$interval($scope.buildStats, 60000);
	$interval(save, 30000);
}]);
