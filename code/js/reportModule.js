
angular.module("reportModule", ["utilitiesModule"])
.directive("reportDirective", [function()
{
  return {
    restrict    : "E",
    templateUrl : "/templates/report.html",
    scope       : {
                    report : "="
                  }
  }
}])
.service("reportService", [function()
{
  var service = {};
  
  service.report = function()
  {
    this.count = 0;
    this.totalms = 0;
    this.averagems = 0;
    this.nonzero = 0;
  }

  service.report.prototype.update = function(task)
  {
    if(task.lapse > 0)
    {
      this.nonzero = this.nonzero + 1;
    }

    this.count     = this.count + 1;
    this.totalms   = this.totalms + task.lapse;
    if(this.nonzero > 0)
    {
      this.averagems = Math.round(this.totalms / this.nonzero);
    }
    else
    {
       this.averagems = 0;
    }
  }

  return service;
}]);