﻿angular.module("statsModule", ["reportModule"])
.directive("statsDirective", [function(){
  return {
    restrict : "E",
    templateUrl : "/templates/stats.html",
    scope : {
      stats : "="
    }
  };
}])
.service("statsService", ["reportService", function(reportService)
  {
    var service = {};
    
    service.stats = function(name, color)
    {
      this.title = name;
      this.color = color;
      this.all = new reportService.report();
      this.correct = new reportService.report();
      this.unchecked = new reportService.report();
      this.incorrect = new reportService.report();
    }

    service.stats.prototype.update = function(task)
    {
      this.all.update(task);
      switch(task.check)
      {
        case 0:
          this.unchecked.update(task);
          break;
        case 1:
          this.correct.update(task);
          break;
        case -1:
          this.incorrect.update(task);
          break;
      };
    };

    return service;
  }]);
