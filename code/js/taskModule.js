'use strict'

window.dateParser = function(key, value)
{
	if(key == "date" && typeof value != "undefined")
	{
		return new Date(value);
	}

	return value;
}

angular.module("taskModule", [])
.directive("taskDirective", ["$interval", function($interval){
	return {
		restrict : "E",
		templateUrl : "/templates/task.html",
		transclude : true,
		scope	: 
		{
			task : "=",
			choosing: "=" 
		},
		controller : function($scope)
		{
			var broadcastChanges = function()
			{
				$scope.$emit("changes-made");
			};

			$scope.editTask = function()
			{
				$scope.task.editing = true;
			};

			$scope.doneEditing = function()
			{
				$scope.task.editing = false;
				broadcastChanges();
			};

			$scope.check = function()
			{
				$scope.task.check = 1;
				broadcastChanges();
			}

			$scope.uncheck = function()
			{
				$scope.task.check = 0;
				broadcastChanges();
			}

			$scope.checkBad = function()
			{
				$scope.task.check = -1;
				broadcastChanges();
			}

			$scope.start = function(_start)
			{
				if(!_start)
				 	_start = new Date();
				$scope.stop($scope.task);
				$scope.task.running = true;
				
				$scope.task._stopwatch = $interval(function(){
					$scope.task.lapse = new Date() - _start;
				}, 10);

				broadcastChanges();
			};

			$scope.stop = function()
			{
				$interval.cancel($scope.task._stopwatch);
				$scope.task.running = false;
				broadcastChanges();
			};

			if($scope.task.running)
			{
				$scope.start(new Date(new Date() - $scope.task.lapse));
			}			
		}
	};	
}])
.service('taskService', ["$q", function taskService($q){
	var service = {};
	service.task = function(name)
	{
		if(typeof name == 'undefined')
		{
			name = "no-name";
		}

		this.name = name;
		this.lapse = 0;
		this.editing = true;
		this.check = 0;
		this.trackers = [];
		this._id = u.random.id();
	};

	service.taskList = function()
	{
		this.tasks = [];
		this.trackers = [];
		this._id = u.random.id();
	};

	service.get = function()
	{
		if(typeof localStorage.tasks != "undefined")
		{
			var current = new service.taskList();
			current.tasks = JSON.parse(localStorage.tasks);
			service.save(current);
			localStorage.removeItem("tasks");
			return current; 
		}
		if(typeof localStorage.current != "undefined")
		{
			return JSON.parse(localStorage.current, dateParser);
		}

		return new service.taskList();
	};

	service.save = function(list)
	{
		localStorage.current = angular.toJson(list);
	};

	var getArchive = function()
	{
		if(typeof localStorage.archive != "undefined")
		{
			return JSON.parse(localStorage.archive, dateParser);
		}

		return [];
	}

	service.archiveList = function(list)
	{
		var deferred = $q.defer();
		var archive = getArchive();
		
		var i = _.findIndex(archive, {_id : list._id});
		if(i == -1)
		{
			archive.push(list);
		}
		else
		{
			archive[i] = list;
		}

		archive = _.sortBy(archive, "date");
		localStorage.archive = angular.toJson(archive);
		deferred.resolve(archive);
		return deferred.promise;		
	};

	return service;	
}]);