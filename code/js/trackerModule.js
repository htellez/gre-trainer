'use strict'


angular.module("trackerModule", [])
.service("trackerService", function()
{
	var service = {};

	service.tracker = function(name, color)
	{
		this.name = name;
		this._id = u.random.id();
		this.color = color;
	}

	service.get = function()
	{
		if(typeof localStorage.trackers != "undefined")
		{
			return angular.fromJson(localStorage.trackers);
		}

		return [];
	}

	service.save = function(trackers)
	{
		localStorage.trackers = angular.toJson(trackers);
	}

	return service;
});