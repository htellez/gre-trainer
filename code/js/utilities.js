window.u = {};
window.u.random = 
{
	id : function()
	{
		var characters = "abcdef01234567890";
		var length = characters.length;
		var id = [];
		for(var i = 0; i < 24; i++)
		{
			id.push(characters[Math.floor(Math.random() * length)]);
		}		

		return id.join("");		
	},

}

window.u.colors = 	 
 {
 	  colors : ["#DD514C", "#F37B1D", "#FAD232", "#1CB841", "#0078E7", "#8058A5",
 	  					"#FF847F", "#FFAE50", "#FFFF65", "#4FEB74", "#42B8DD", "#B38BD8"],
 	  get : function(index)
 	  {
 	  	return u.colors.colors[index % u.colors.colors.length];
 	  },
 	  random : function()
		{
			var colors = u.colors.colors; 
			return colors[Math.floor(Math.random() * colors.length)];
		}
 }