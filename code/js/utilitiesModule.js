angular.module("utilitiesModule", [])
.filter("timespan", function()
  {
    return function(ms)
    {
      var seconds = Math.floor(ms/1000);
      var miliseconds = ms - (1000*seconds)

      var minutes = Math.floor(seconds / 60);
      seconds = seconds - (minutes*60);

      var hours = Math.floor(minutes / 60);
        minutes = minutes - (60*hours);
        
        var prefix = "s";
        var result = seconds;
        if(minutes == 0 && hours == 0)
        {
          var padding = "";
          if(miliseconds < 10)
          {
            padding = "00";
          }
          if(miliseconds < 100)
          {
            padding = "0";
          }

          result = result + "." + padding + miliseconds;
        }
        if(seconds < 10 && minutes > 0)
        {
          result = "0" + result;
        }
        if(minutes > 0)
        {
          prefix = "m";
          result = minutes + ":" + result;
          if(minutes < 10 && hours > 0)
          {
            result = "0" + result;
          }
        }

        if(hours > 0)
        {
          prefix = "h";
          result = hours + ":" + result;
        }

        return result + prefix
    };
  })
  .filter("totalms", [function()
    {
      return function(tasks)
      {
        var total = 0;
        tasks.forEach(function(t){total = total + t.lapse;});
        return total
      }
    }])
  .filter("averagems", [function()
    {
      return function(tasks)
      {
        if(tasks.length == 0)
        {
          return 0;
        }
        
        var total = 0;
        tasks.forEach(function(t){total = total + t.lapse;});
        return Math.round(total/tasks.length);        
      }
    }])